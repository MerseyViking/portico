# Copyright (c) 2014 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
from datetime import datetime, timedelta
import os
import gzip
from cStringIO import StringIO
import zipfile

import requests
from flask import render_template, Response, jsonify, request, abort
from flask.helpers import make_response
from flask.json import JSONEncoder
from sqlalchemy import func

import json

import winged_edge

from models import amon
from app import app, session


# import logging
# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


class DateTimeJSONEncoder(JSONEncoder):
    def default(self, o):
        try:
            if isinstance(o, datetime):
                return o.__str__()

            iterable = iter(o)
        except TypeError:
            pass
        else:
            return list(iterable)

        return JSONEncoder.default(self, o)


_json_encoder = DateTimeJSONEncoder()


@app.route('/')
@app.route('/thermoview.html')
def thermoview():
    return render_template('thermoview.html')


@app.route('/graphs.html')
def graphs():
    return render_template('graphs.html')


@app.route('/settings.html')
def settings():
    return render_template('settings.html')


@app.route('/housahedron/scene/<model_id>', methods=['GET'])
def get_clara_scene(model_id):
    username = 'GeoSpark'
    api_key = '64f741a7-1fc1-4dba-9fa2-909f9a93e0bc'
    base_url = 'http://clara.io/api'
    file_name = '.cache/{0}.json.gz'.format(model_id)
    download_scene = True
    scene = StringIO()

    try:
        if os.path.isfile(file_name):
            r = requests.get('{0}/users/{1}/scenes'.format(base_url, username), auth=(username, api_key))
            scenes = r.json()
            # TODO: Deal with paging.
            for model in scenes['models']:
                if model['id'] == model_id:
                    file_time = datetime.utcfromtimestamp(os.path.getmtime(file_name))
                    modified_time = datetime.strptime(model['modifiedAt'], '%Y-%m-%dT%H:%M:%S.%fZ')

                    if file_time >= modified_time:
                        download_scene = False

                    break

        if download_scene:
            url = '{0}/scenes/{1}/export/json'.format(base_url, model_id)
            r = requests.get(url, params={'zip': str(False).lower()}, auth=(username, api_key))

            if r.status_code != 200:
                return Response(response=r.text, status=r.status_code, headers=r.headers)

            if r.headers['content-type'] == 'application/zip':
                zipped_content = StringIO()
                zipped_content.write(r.content)
                with zipfile.ZipFile(file=zipped_content, mode='r') as zipped_scene:
                    scene_contents = zipped_scene.open(zipped_scene.infolist()[0])
            else:
                scene_contents = StringIO()
                scene_contents.write(r.content)
                scene_contents.seek(0, 0)

            with gzip.GzipFile(file_name, mode='wb', fileobj=scene) as tt:
                tt.write(scene_contents.read())

            with open(file_name, 'wb') as f:
                f.write(scene.getvalue())
        else:
            with open(file_name, 'r') as f:
                scene.write(f.read())
    except IOError:
        with open(file_name, 'r') as f:
            scene.write(f.read())

    return Response(response=scene.getvalue(), status=200, mimetype='application/json',
                    headers={'Content-Encoding': 'gzip', 'Vary': 'Accept-Encoding'})


@app.route('/housahedron/edges/<model_id>', methods=['GET'])
def get_edges(model_id):
    g = winged_edge.find_edges(model_id)
    j = json.dumps(g)
    return Response(response=j, status=200, mimetype='application/json')


@app.route('/housahedron/data', methods=['GET'])
def get_data():
    try:
        start = datetime.strptime(request.args.get('start'), '%Y-%m-%dT%H:%M:%S.%fZ')
        start_timestamp = (start - datetime(1970, 1, 1)).total_seconds()

        end = datetime.strptime(request.args.get('end'), '%Y-%m-%dT%H:%M:%S.%fZ')
        end_timestamp = (end - datetime(1970, 1, 1)).total_seconds()

        r = session.query(amon.Measurement.device_id, func.max(amon.Measurement.timestamp), amon.Measurement.value).\
            filter(amon.Measurement.timestamp >= start_timestamp, amon.Measurement.timestamp <= end_timestamp).\
            group_by(amon.Measurement.device_id).\
            order_by(amon.Measurement.device_id).all()

        x = json.dumps({'results': r}, default=_json_encoder.default)
        response = make_response(x)
        response.headers['Content-Type'] = 'application/json'
        return response
    except ValueError:
        # The given datetime is in the wrong format.
        abort(400)
    except TypeError:
        # No datetime has been provided.
        abort(400)


@app.route('/housahedron/weather', methods=['GET'])
def get_weather():
    try:
        start = datetime.strptime(request.args.get('start'), '%Y-%m-%dT%H:%M:%S.%fZ')

        r = session.query(amon.WeatherObservation.observation_time, amon.WeatherObservation.temperature, amon.WeatherObservation.weather).\
            filter(amon.WeatherObservation.observation_time <= start).\
            order_by(amon.WeatherObservation.observation_time.desc()).\
            limit(1).all()

        x = json.dumps({'results': r}, default=_json_encoder.default)
        response = make_response(x)
        response.headers['Content-Type'] = 'application/json'
        return response
    except ValueError as e:
        # The given datetime is in the wrong format.
        abort(400)
    except TypeError as e:
        # No datetime has been provided.
        abort(400)


@app.route('/_sensor_network', methods=['GET'])
def sensor_network():
    # TODO: Take time parameter or now.
    sub_query = session.query(amon.Measurement.device_id, amon.Measurement.value, func.max(amon.Measurement.timestamp).label('ts')).\
        group_by(amon.Measurement.device_id).subquery()

    results = session.query(amon.Entity.uuid,
                            amon.Entity.description,
                            amon.MeteringPoint.uuid,
                            amon.MeteringPoint.description,
                            amon.Device.uuid,
                            amon.Measurement.reading_type,
                            amon.Measurement.value,
                            sub_query.c.value - amon.Measurement.value,
                            sub_query.c.ts,
                            func.max(amon.Measurement.timestamp)).\
        select_from(amon.Measurement).\
        join(sub_query, sub_query.c.ts > amon.Measurement.timestamp).\
        join(amon.MeteringPoint, amon.MeteringPoint.uuid == amon.Device.metering_point_id).\
        join(amon.Device, amon.Device.uuid == amon.Measurement.device_id).\
        join(amon.Entity, amon.Entity.uuid == amon.MeteringPoint.entity_id).\
        filter(amon.Measurement.device_id == sub_query.c.device_id).\
        group_by(amon.Measurement.device_id)

    tree = {}

    for result in results:
        device = {'id': result[4], 'number': int(result[4][-2:]), 'type': result[5], 'value': result[6],
                  'trend': result[7]}
        m = {'id': result[2], 'last_updated': result[8], 'description': result[3], 'devices': []}
        e = {'id': result[0], 'description': result[1], 'metering_points': {}}

        if result[0] not in tree:
            tree[result[0]] = e

        if result[2] not in tree[result[0]]['metering_points']:
            tree[result[0]]['metering_points'][result[2]] = m

        tree[result[0]]['metering_points'][result[2]]['devices'].append(device)

    return jsonify(tree)