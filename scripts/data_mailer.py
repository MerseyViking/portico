# Copyright (c) 2014 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
import os
import sys
import base64
from datetime import datetime, timedelta
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
import sqlite3
import smtplib
import cStringIO
import csv
import gzip
import oauth2 as oauth
import logging

import yaml


def do_it(file_name):
    pp = os.path.dirname(os.path.abspath(__file__))
    try:
        config = yaml.safe_load(open(os.path.join(pp, file_name), 'r'))
    except yaml.YAMLError as e:
        logging.error(e)
        return

    try:
        with open(os.path.join(pp, config['secret_path']), 'r') as f:
            client_secret = f.readline()
    except IOError as e:
        logging.error(e)
        return

    conn = sqlite3.connect(os.path.join(pp, config['database']))
    cur = conn.cursor()

    logging.info('Generating authorization token')
    authorization = oauth.RefreshToken(config['client_id'], client_secret, config['refresh_token'])
    oauth2_string = 'user={0}\1auth={1} {2}\1\1'.format(config['from'], authorization['token_type'],
                                                        authorization['access_token'])

    msg = MIMEMultipart()
    msg['Date'] = formatdate(localtime=False)
    msg['Message-Id'] = make_msgid()
    yesterday = datetime.utcnow() - timedelta(days=1)
    msg['Subject'] = '{0} for {1}'.format(config['title'], yesterday.strftime('%d %B, %Y'))
    msg['From'] = config['from']
    msg['Bcc'] = ', '.join(config['recipients'])

    message_body = 'Query results:\n'

    for r in config['results']:
        logging.debug("Query '{0}' {1}".format(r['name'], r['query']))
        data = cStringIO.StringIO()
        cur.execute(r['query'])

        with gzip.GzipFile(r['name'] + '.csv', mode='wb', fileobj=data) as tt:
            writer = csv.writer(tt)
            writer.writerow([x[0] for x in cur.description])
            rows = cur.fetchall()
            row_count = len(rows)
            writer.writerows(rows)

        data.seek(0, 0)
        attachment = MIMEApplication(data.read(), _subtype='x-gzip')
        msg.attach(attachment)
        attachment.add_header('Content-Disposition', 'attachment; filename="{0}.csv.gz"'.format(r['name']))
        data.close()

        message_body += "    '{0}' returned {1} rows\n".format(r['name'], row_count)

    msg.attach(MIMEText(message_body))

    logging.info('Connecting and authorizing')
    s = smtplib.SMTP('smtp.gmail.com')
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.docmd('AUTH', 'XOAUTH2 ' + base64.b64encode(oauth2_string))

    logging.info('Sending')
    s.sendmail(msg['From'], config['recipients'], msg.as_string())
    s.quit()

if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s.%(msecs).03dZ|%(levelname)s|%(message)s',
                        level=logging.INFO,
                        datefmt='%Y-%m-%dT%H:%M:%S')
    logging.info('Start gathering data with {0}'.format(sys.argv[1]))
    do_it(sys.argv[1])
    logging.info('Finished gathering data')
