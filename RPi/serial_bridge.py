#!/usr/bin/env python

# Heavily modified by John Donovan from 'tcp_serial_redirect.py':
# (C) 2002-2009 Chris Liechti <cliechti@gmx.net>
# redirect data from a TCP/IP connection to a serial port and vice versa
# requires Python 2.2 'cause socket.sendall is used


import sys
import time
import threading
import socket
import codecs
import serial

import RPi.GPIO as GPIO


class Redirector(object):
    def __init__(self, serial_instance, sock, spy=False):
        self.serial = serial_instance
        self.socket = sock
        self.spy = spy
        self._write_lock = threading.Lock()
        self.alive = False
        self.thread_read = None

    def shortcut(self):
        """connect the serial port to the TCP port by copying everything
           from one side to the other"""
        self.alive = True
        self.thread_read = threading.Thread(target=self.reader)
        self.thread_read.setDaemon(True)
        self.thread_read.setName('serial->socket')
        self.thread_read.start()
        self.writer()

    def reader(self):
        """loop forever and copy serial->socket"""
        while self.alive:
            try:
                data = self.serial.read(1)              # read one, blocking
                if len(data) == 0:
                    continue

                packet_length = int(data.encode('hex'), 16)
                data = data + self.serial.read(packet_length - 1)   # and get as much as possible

                if data:
                    # the spy shows what's on the serial port, so log it before converting newlines
                    if self.spy:
                        sys.stdout.write(codecs.escape_encode(data)[0])
                        sys.stdout.flush()

                    self._write_lock.acquire()
                    try:
                        self.socket.sendto(data, ('localhost', 1883))           # send it over TCP
                    finally:
                        self._write_lock.release()
            except socket.error as ex:
                sys.stderr.write('1ERROR: %s\n' % ex)
                # probably got disconnected
                break
        self.alive = False

    def writer(self):
        """loop forever and copy socket->serial"""
        while self.alive:
            try:
                data = self.socket.recv(1024)
                if not data:
                    break

                self.serial.write(data)                 # get a bunch of bytes and send them
                # the spy shows what's on the serial port, so log it after converting newlines
                if self.spy:
                    sys.stdout.write(codecs.escape_encode(data)[0])
                    sys.stdout.flush()
            except socket.error, ex:
                sys.stderr.write('2ERROR: %s\n' % ex)
                # probably got disconnected
                break

        self.alive = False
        self.thread_read.join()

    def stop(self):
        """Stop copying"""
        if self.alive:
            self.alive = False
            self.thread_read.join()


if __name__ == '__main__':
    import optparse

    parser = optparse.OptionParser(
        usage="%prog [options] [port [baudrate]]",
        description="Simple Serial to Network (TCP/IP) redirector.",
        epilog="""\
NOTE: no security measures are implemented. Anyone can remotely connect
to this service over the network.

Only one connection at once is supported. When the connection is terminated
it waits for the next connect.
""")

    parser.add_option("--spy",
                      dest="spy",
                      action="store_true",
                      help="peek at the communication and print all data to the console",
                      default=False)

    group = optparse.OptionGroup(parser, "Serial Port", "Serial port settings")
    parser.add_option_group(group)

    group.add_option("-p", "--port",
                     dest="port",
                     help="port, a number (default 0) or a device name",
                     default='/dev/ttyAMA0')

    group.add_option("-b", "--baud",
                     dest="baudrate",
                     action="store",
                     type='int',
                     help="set baud rate, default: %default",
                     default=115200)

    group = optparse.OptionGroup(parser, "Network settings", "Network configuration.")
    parser.add_option_group(group)

    group.add_option("-P", "--localport",
                     dest="local_port",
                     action="store",
                     type='int',
                     help="local TCP port",
                     default=1337)

    (options, args) = parser.parse_args()

    # get port and baud rate from command line arguments or the option switches
    port = options.port
    baudrate = options.baudrate
    if args:
        if options.port is not None:
            parser.error("no arguments are allowed, options only when --port is given")
        port = args.pop(0)
        if args:
            try:
                baudrate = int(args[0])
            except ValueError:
                parser.error("baud rate must be a number, not %r" % args[0])
            args.pop(0)
        if args:
            parser.error("too many arguments")
    else:
        if port is None:
            port = 0

    GPIO.setmode(GPIO.BOARD)
    reset_pin = 12
    GPIO.setup(reset_pin, GPIO.OUT)
    GPIO.output(reset_pin, GPIO.HIGH)
    time.sleep(0.12)
    GPIO.output(reset_pin, GPIO.LOW)

    # connect to serial port
    ser = serial.Serial()
    ser.port = port
    ser.baudrate = baudrate
    ser.parity = 'N'
    ser.rtscts = False
    ser.xonxoff = False
    ser.timeout = 1     # required so that the reader thread can exit

    try:
        ser.open()
    except serial.SerialException, e:
        sys.stderr.write("Could not open serial port %s: %s\n" % (ser.portstr, e))
        sys.exit(1)

    srv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    srv.bind(('', options.local_port))

    while True:
        try:
            sys.stderr.write("Waiting for connection on %s...\n" % options.local_port)
            r = Redirector(
                ser,
                srv,
                options.spy
            )
            r.shortcut()
            if options.spy:
                sys.stdout.write('\n')

            sys.stderr.write('Disconnected\n')
        except KeyboardInterrupt:
            break
        except socket.error, msg:
            sys.stderr.write('3ERROR: %s\n' % msg)

    sys.stderr.write('\n--- exit ---\n')
