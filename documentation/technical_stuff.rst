Housahedron Technical Overview
==============================

Hardware
--------
The hardware uses ATMega-328P based microcontrollers, which are those found on
Arduino Unos and similar.

Database
--------

Frontend
--------

3D Renderer
~~~~~~~~~~~
The only practical solution for 3D in a modern web browser is WebGL_. It is
mostly supported by all modern browsers_, and uses the OpenGL ES specification.
The emerging *de-facto* library that wraps WebGL is `three.js`_, which provides a
scene graph, geometry and maths functions, mesh loaders, and so on.

3D Model
~~~~~~~~
Currently we use `clara.io`_ as our 3D model repository and editor. It has a web
API which enables us to download the latest version of a model into our
interface. clara.io has the ability to export the mesh as a three.js-formatted
JSON file which can be loaded directly into our renderer.

Housahedron's mesh loader works in two stages. The Javascript code makes a
``GET`` request to the server for the model with a given UUID. The server first
checks to see if this model has been downloaded from clara.io before; if it
has, then it checks there isn't a more up-to-date version by making a ``GET``
request to clara.io asking for the last-modified date for that model. If the
model hasn't been downloaded before, or there is a more recent one, the model
is downloaded, GZipped, and stored on the server in the ``.cache`` directory.

The Javascript mesh loader does some extra processing on the incoming model to
prepare it for rendering the heatmap. In clara.io, logical elements such as
sensors are represented by 'null' objects - these have position, attributes,
and so on, but no actual geometry associated with them. ``room`` elements are slightly
different in that they are geometry that ``metering_point`` elements are attached to.
In addition, a call is made to generate a wireframe of the building mesh which
is overlayed on the model to give the edges some definition.

The heirarchy of nodes required[*]_ in a scene look like this:

* entity
  * room
    * metering point
      * device
      * device
      * ...

    * metering point
    * metering point
    * ...

  * room
    * metering point

  * ...

and is based on the `AMON`_ standard.

Each element has a number of metadata attributes associated with them. These
are in the form of key-value pairs and are required by the loader to be a part
of the scene. The minimum attributes are ``class`` and ``id``, where ``class``
depends on the element type, and ``id`` is a `version 4 UUID`_. An optional
``description`` attribute can be specified which is a free-form text
description of the element.

Entity
......
Currently there can be only one entity per scene, but this will
change in the future.

+-----------------+----------+-----------------------------------------+
| Attribute       | Required | Notes                                   |
+=================+==========+=========================================+
| ``class``       | yes      | Must be ``entity``                      |
+-----------------+----------+-----------------------------------------+
| ``id``          | yes      | Any UUID. This must match the UUID      |
|                 |          | stored in the database for that entity. |
+-----------------+----------+-----------------------------------------+
| ``description`` | no       |                                         |
+-----------------+----------+-----------------------------------------+

Room
....
This is the actual geometry that is rendered in the browser. Each room is
independent to speed up the rendering of the heatmap. So if there are no
sensors on the other side of a wall, that side won't be shaded. Rooms can be
fairly conceptual, for instance an open office can have several rooms off the
main area that don't have walls or doors separating them; these should be
considered rooms. One potential issue with this is that if there's a sensor
near the join between two rooms, the shading will not 'cross over' to the other
room, so there will be a noticable seam.

+-----------------+----------+------------------+
| Attribute       | Required | Notes            |
+=================+==========+==================+
| ``class``       | yes      | Must be ``room`` |
+-----------------+----------+------------------+
| ``id``          | no       | Any UUID.        |
+-----------------+----------+------------------+
| ``description`` | no       |                  |
+-----------------+----------+------------------+

Metering point
..............
Within the Housahedron system, a metering point represents a
JeeNode, Moteino, or some other microcontroller. It gathers the data from the
sensors connected to it, turns it into an MQTT-SN packet and broadcasts it.

+-----------------+----------+-------------------------------------------------+
| Attribute       | Required | Notes                                           |
+=================+==========+=================================================+
| ``class``       | yes      | Must be ``metering_point``                      |
+-----------------+----------+-------------------------------------------------+
| ``id``          | yes      | A version 4 UUID. Metering point UUIDs have the |
|                 |          | form ``xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx``   |
|                 |          | where ``x`` is any hexadecimal digit and ``y``  |
|                 |          | must be ``8``                                   |
+-----------------+----------+-------------------------------------------------+
| ``description`` | no       |                                                 |
+-----------------+----------+-------------------------------------------------+

Device
......
Devices are the sensors themselves.

+-----------------+----------+------------------------------------------------+
| Attribute       | Required | Notes                                          |
+=================+==========+================================================+
| ``class``       | yes      | Must be ``device``                             |
+-----------------+----------+------------------------------------------------+
| ``id``          | yes      | A version 4 UUID. Device UUIDs have the        |
|                 |          | form ``xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxzz``  |
|                 |          | where ``x`` is any hexadecimal digit, ``y``    |
|                 |          | must be ``9``, and ``zz`` is a sequential      |
|                 |          | number from 0. Currently the remaining parts   |
|                 |          | of the UUID match the parent metering point,   |
|                 |          | but this isn't a requirement.                  |
+-----------------+----------+------------------------------------------------+
| ``type``        | yes      | Can be any one of the types supported by AMON  |
|                 |          | v3.1, but currently only ``temperatureAir`` is |
|                 |          | recognised.                                    |
+-----------------+----------+------------------------------------------------+
| ``privacy``     | no       | If present must be one of ``private`` or       |
|                 |          | ``public``, but this attribute is currently    |
|                 |          | ignored.                                       |
+-----------------+----------+------------------------------------------------+
| ``description`` | no       |                                                |
+-----------------+----------+------------------------------------------------+

When the model is downloaded by the system, it iterates over the scene graph
and extracts the ``room`` geometry, and pushes each mesh onto an array. The
meshes then have their materials replaced with a custom shader that does the
interpolation between the sensors.

The devices are then parsed, and their ids, types, and positions are added to a
dictionary. For each metering point and device a request is made to the server
to ensure the model matches the database.


.. [*] Although it would be a good idea to decouple the logical elements from the
  geometry and just use references. This allows us to store the logical structure
  in a different format and/or location.

.. _`three.js`: http://threejs.org
.. _WebGL: http://get.webgl.org
.. _browsers: http://caniuse.com/#feat=webgl
.. _`clara.io`: https://clara.io
.. _`version 4 UUID`:
   https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29
.. _`AMON`: https://amee.github.io/AMON/
