CREATE TABLE entities (
	uuid VARCHAR(36) NOT NULL, 
	description TEXT, 
	PRIMARY KEY (uuid), 
	UNIQUE (uuid)
);
;
CREATE TABLE observations (
	_id INTEGER NOT NULL, 
	latitude FLOAT NOT NULL, 
	longitude FLOAT NOT NULL, 
	elevation FLOAT NOT NULL, 
	observation_time DATETIME NOT NULL, 
	weather TEXT NOT NULL, 
	temperature FLOAT NOT NULL, 
	rel_humidity INTEGER NOT NULL, 
	wind_dir INTEGER NOT NULL, 
	wind_speed FLOAT NOT NULL, 
	wind_gust FLOAT NOT NULL, 
	pressure FLOAT NOT NULL, 
	dewpoint FLOAT NOT NULL, 
	precip INTEGER NOT NULL, 
	PRIMARY KEY (_id), 
	UNIQUE (_id)
);
;
CREATE INDEX ix_observations_observation_time ON observations (observation_time);
CREATE TABLE metering_points (
	uuid VARCHAR(36) NOT NULL, 
	description TEXT, 
	entity_id VARCHAR(36) NOT NULL, 
	x FLOAT NOT NULL, 
	y FLOAT NOT NULL, 
	z FLOAT NOT NULL, 
	is_placed BOOLEAN NOT NULL, 
	battery_status INTEGER NOT NULL, 
	json_metadata TEXT, 
	PRIMARY KEY (uuid), 
	UNIQUE (uuid), 
	FOREIGN KEY(entity_id) REFERENCES entities (uuid), 
	CHECK (is_placed IN (0, 1))
);
;
CREATE INDEX ix_metering_points_entity_id ON metering_points (entity_id);
CREATE TABLE devices (
	uuid VARCHAR(36) NOT NULL, 
	metering_point_id VARCHAR(36) NOT NULL, 
	description TEXT, 
	privacy VARCHAR(7) NOT NULL, 
	x FLOAT NOT NULL, 
	y FLOAT NOT NULL, 
	z FLOAT NOT NULL, 
	is_placed BOOLEAN NOT NULL, 
	PRIMARY KEY (uuid), 
	UNIQUE (uuid), 
	FOREIGN KEY(metering_point_id) REFERENCES metering_points (uuid), 
	CHECK (privacy IN ('private', 'public')), 
	CHECK (is_placed IN (0, 1))
);
;
CREATE INDEX ix_devices_metering_point_id ON devices (metering_point_id);
CREATE TABLE measurements (
	_id INTEGER NOT NULL, 
	reading_type TEXT NOT NULL, 
	timestamp DATETIME NOT NULL, 
	value FLOAT NOT NULL, 
	error TEXT, 
	aggregated BOOLEAN, 
	device_id VARCHAR(36) NOT NULL, 
	PRIMARY KEY (_id), 
	UNIQUE (_id), 
	CHECK (aggregated IN (0, 1)), 
	FOREIGN KEY(device_id) REFERENCES devices (uuid)
);
;
CREATE INDEX ix_measurements_timestamp ON measurements (timestamp);
CREATE INDEX ix_measurements_device_id ON measurements (device_id);
