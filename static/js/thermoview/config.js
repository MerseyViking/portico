// Configure Require.js
var require = {
  // Default load path for js files
  baseUrl: 'static/js/thermoview/app',
  shim: {
    'threeDebug': {exports: 'THREE'},
    'threeCore': {exports: 'THREE'},
    'OrbitControls': {deps: ['threeCore'], exports: 'THREE'},
    'detector': {exports: 'Detector'},
//    'PP': {deps: ['threeCore'], exports: 'PP'},
//    'PPDebug': {deps: ['threeCore'], exports: 'PP'},
    'tween': {exports: 'TWEEN'},
    'suncalc': {exports: 'SunCalc'},
    'sparklines': {exports: 'SparkLines'}
  },

  paths: {
    threeDebug: '../lib/three',
    threeCore: '../lib/three.min',
    OrbitControls: '../lib/OrbitControls',
    text: '../lib/text',
    detector: '../lib/Detector',
    suncalc: '../lib/suncalc',
//    PP: '../lib/PP',
//    PPDebug: '../lib/PPDebug',
    tween: '../lib/tween.min',
    shader: '../lib/shader',
    shaders: '../shaders',
    sparklines: '../lib/jquery.sparkline.min'
  }
};
