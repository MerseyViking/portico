#define PRECIS mediump

varying PRECIS vec3 world_pos;
varying PRECIS float alpha;
uniform PRECIS vec3 centre;
uniform PRECIS float radius;
uniform bool selected;
uniform bool highlighted;
uniform PRECIS float pulse;

void main(void) {
    vec4 model_pos = modelMatrix * vec4(position, 1.0);
    world_pos = (((model_pos.xyz - centre) / radius) + 1.0) / 2.0;

    vec4 mvPosition = modelViewMatrix * vec4(position, 1.0);

    if (selected) {
        alpha = 1.0;
    } else if (highlighted) {
        alpha = pulse;
    } else {
        alpha = 0.1;
    }

    gl_Position = projectionMatrix * mvPosition;
}
