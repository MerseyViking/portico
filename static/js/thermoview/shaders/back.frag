#define PRECIS mediump
#define MAX_SENSORS 128
#define MAX_ALPHA_FALLOFF 1.0

varying PRECIS vec3 world_pos;
varying PRECIS float alpha;
//uniform PRECIS float p;
uniform sampler2D gradient;
uniform sampler2D sensor_positions;
uniform sampler2D sensor_values;
uniform PRECIS int num_sensors;
uniform PRECIS float room_texture_v;
uniform bool selected;

void main() {
    if (selected && !gl_FrontFacing) {
        discard;
    }

    float weight_sum = 0.0;
    float value = 0.0;
    float min_dist_sq = 1.0;

    vec4 no_sensor_coverage_colour = vec4(1.0, 1.0, 1.0, 1.0);

    for (int i = 0; i < MAX_SENSORS; ++i) {
        // It seems OpenGL ES struggles with non-constant-sized loops.
        if (i >= num_sensors) {
            break;
        }

        vec2 sensor_coord = vec2((float(i) + 0.5) / float(MAX_SENSORS), room_texture_v);

        vec3 node = texture2D(sensor_positions, sensor_coord).xyz;
        vec3 rel_pos = world_pos - node;
        float distance_sq = dot(rel_pos, rel_pos);
        min_dist_sq = min(min_dist_sq, distance_sq);
        float weight = 1.0 / distance_sq;
        //weight = pow(distance_sq, -3.0);
        weight_sum += weight;
        value += (weight * texture2D(sensor_values, sensor_coord).x);
    }

    float distance_blend = smoothstep(0.0, 1.0, sqrt(min_dist_sq) / MAX_ALPHA_FALLOFF);

    if (distance_blend < 1.0) {
        value /= weight_sum;
        gl_FragColor = mix(texture2D(gradient, vec2(value, 0.5)), no_sensor_coverage_colour, distance_blend);
    } else {
        gl_FragColor = no_sensor_coverage_colour;
    }

    gl_FragColor.a = alpha;
}