define(['threeCore', 'container', 'camera', 'renderer', 'scene', 'controls', 'grid', 'hud', 'sky', 'tween'],
    function(THREE, container, camera, renderer, scene, controls, grid, hud) {
        var app = {
            meshes: [],
            init: function() {
//                scene.load('a372cb2e-5569-4742-928c-b7a31293b274');
                scene.load('aafaffe1-fe43-4b99-b693-a6013011b3f7');
//                scene.load('84ad62a6-116e-4e16-bb82-3b5bf43155bd');
            },
            animate: function() {
                window.requestAnimationFrame(app.animate);
                TWEEN.update();
                controls.update();
                hud.update();
//                PP.start();
//                PP.renderScene().toTexture('tDepth');
//                PP.get('unsharpMasking').set('tDepth').toTexture('tDepth');
                renderer.render(scene.the_scene, camera);
            }
        };
        return app;
    }
);
