define(['scene', 'suncalc'],
    function(scene, suncalc) {
        var sky_box = null;

        var sky_shader = function () {
            var vertexshader = "\
            varying vec2 vUv;\
            void main() {\
                vUv = uv;\
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\
            }";

            var fragmentshader = "\
            varying vec2 vUv;\
            void main() {\
                float v = length(vUv.st - vec2(0.5, 0.5)) * 2.0;\
                float a = smoothstep(0.045, 0.155, v);\
                gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0 - a);\
            }";

            return new THREE.ShaderMaterial({
                transparent: true,
                depthTest: true,
                depthWrite: true,
                vertexShader: vertexshader,
                fragmentShader: fragmentshader,
                side: THREE.BackSide});
        }();

        $(scene).on('date_changed', function(event, date) {
            if (sky_box !== null) {
                var sun_pos = suncalc.getPosition(date, 52.956391, -0.9525);
                sky_box.translateZ(-4000.0);
                // scene.sky_box.quaternion.setFromEuler(new THREE.Euler(-sun_pos.altitude, -sun_pos.azimuth, 0.0, "YXZ"));
                sky_box.quaternion.setFromEuler(new THREE.Euler(-sun_pos.altitude, -sun_pos.azimuth - Math.PI / 2.0, 0.0, "YXZ"));

                sky_box.translateZ(4000.0);
            }
        });

        $(scene).on('reloaded', function() {
            if (sky_box === null) {
                var skyGeo = new THREE.PlaneGeometry(4000, 4000, 1, 1, 1);

                sky_box = new THREE.Mesh(skyGeo, sky_shader);
                sky_box.translateZ(4000.0);
            }

            scene.the_scene.add(sky_box);
        });
    }
);