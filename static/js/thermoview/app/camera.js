define(['threeCore', 'container'], function(THREE, container) {
    var camera = new THREE.PerspectiveCamera(70.0, 1.0, 1.0, 5000.0);
    camera.position.y = 2.0;
    camera.position.z = 27.0;

    var updateSize = function() {
        camera.aspect = container.offsetWidth / container.offsetHeight;
        camera.updateProjectionMatrix();
    };

    window.addEventListener('resize', updateSize, false);
    updateSize();

    return camera;
} );