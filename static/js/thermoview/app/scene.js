define(['threeCore', 'textures', 'materials', 'controls', 'tween'],
    function(THREE, textures, materials, controls) {
        var pulse = {value: 0.0};
        var selected_object;
        var highlighted_object;

        var sensor_icons = {
            'temperatureAir': 'fa-fire'
        };

        var sensor_units = {
            'temperatureAir': '&deg;C'
        };

        var trend_icons = ['fa-arrow-circle-down', 'fa-arrow-circle-right', 'fa-arrow-circle-up'];

        var start_date = new Date(2014, 5, 6, 18, 0, 0, 0);
        var end_date = new Date(2014, 8, 9, 16, 0, 0, 0);

        setInterval(function() {
            if (scene.playing) {
                var date = new Date(scene.next_date.getTime() + (5 * 60000));
                scene.set_date(date);
            }
        }, 500);

        function pulse_fn() {
            var tween_mode;
            var pulse_end;

            if (pulse.value === 1.0) {
                pulse_end = {value: 0.0};
                tween_mode = TWEEN.Easing.Quadratic.In;
            } else {
                pulse_end = {value: 1.0};
                tween_mode = TWEEN.Easing.Quadratic.Out;
            }

            var tween = new TWEEN.Tween(pulse).to(pulse_end, 1500);
            tween.onComplete(pulse_fn);
            tween.onUpdate(function() {
                if (highlighted_object !== undefined) {
                    highlighted_object.material.uniforms.pulse.value = pulse.value;
                }
            });
            tween.easing(tween_mode);
            tween.start();
        }

        pulse_fn();

        function _parse_entity(mesh) {
            if (mesh.userData.class === 'entity') {
                $.ajax('/amon/entities', {
                    type: 'POST',
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify({'entityId': mesh.uuid, 'description': mesh.userData.description}),
                    success: function() {console.log('Done!')}
                });

                var sensor_heirarchy = '<ul class="nav"><li class="parent_li" id="' + mesh.uuid + '">';
                sensor_heirarchy += '<span class="btn-success entity"><i class="fa fa-fw fa-lg fa-building-o"></i> ' + mesh.userData.description + '</span>';
                sensor_heirarchy += '</li></ul>';
                $('#sensor-heirarchy').append(sensor_heirarchy);
                _parse_rooms(mesh, $('#' + mesh.uuid));
                mesh.geometry.dynamic = true;
            }
        }

        function _parse_rooms(mesh, parent) {
            // Because rooms aren't a concept in the AMON standard, we don't need to make a call to the database.
            for (var room_idx = 0; room_idx < mesh.children.length; room_idx++) {

                var room_mesh = mesh.children[room_idx];

                if (room_mesh.userData.class === 'room') {
                    var id = 'roomRadio-' + room_idx;
                    var sensor_heirarchy = '<ul><li class="parent_li" id="' + room_mesh.uuid + '">';
                    sensor_heirarchy += '<input type="radio" name="roomRadio" class="roomRadio" id="' + id + '" value="' + room_idx + '"></input><label class="btn-success room" for="' + id + '"><i class="fa fa-fw fa-lg fa-cube"></i> ' + room_mesh.userData.description + '</label>';
                    sensor_heirarchy += '</li></ul>';

                    parent.append(sensor_heirarchy);
                    _parse_metering_points(room_mesh, $('#' + room_mesh.uuid));
                    room_mesh.renderDepth = 1;
                    scene.room_meshes.push(room_mesh);
                }
            }
        }

        function _parse_metering_points(mesh, parent) {
            var room_sensor_positions = [];

            for (var metering_point_idx = 0; metering_point_idx < mesh.children.length; metering_point_idx++) {
                var metering_point_mesh = mesh.children[metering_point_idx];

                if (metering_point_mesh.userData.class === 'meteringPoint') {
                    $.ajax('/amon/metering-points', {
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        data: JSON.stringify({'meteringPointId': metering_point_mesh.userData.id, 'entityId': mesh.parent.uuid, 'description': metering_point_mesh.userData.description}),
                        success: function() {console.log('Added new metering point to database.')}
                    });

                    var ts = '32 seconds';
                    var sensor_heirarchy = '<ul><li class="parent_li" id="' + metering_point_mesh.uuid + '">';
                    sensor_heirarchy += '<span class="btn-success metering-point" id="' + metering_point_mesh.userData.id + '"><i class="fa fa-fw fa-lg fa-code-fork"></i> ' + metering_point_mesh.userData.description + '</br>' + ts + ' ago</span>';
                    sensor_heirarchy += '</li></ul>';
                    parent.append(sensor_heirarchy);
                    var sensors = _parse_devices(metering_point_mesh, $('#' + metering_point_mesh.uuid));

                    room_sensor_positions = room_sensor_positions.concat(sensors);
//                    $.extend(room_sensor_positions, sensors);
                }
            }

            scene.sensor_pos[parent.attr('id')] = room_sensor_positions;
        }

        function _parse_devices(mesh, parent) {
            var sensor_heirarchy = '<ul>';

            var temperatureAir = [];

            for (var device_idx = 0; device_idx < mesh.children.length; device_idx++) {
                var device_mesh = mesh.children[device_idx];

                if (device_mesh.userData.class === 'device') {
                    // Concatenate the hierarchy's translations. Need to do something more robust if we want
                    // to take into account rotation and scaling.
                    var device_position = device_mesh.position.addVectors(device_mesh.position, mesh.position);
                    var device_id = device_mesh.userData.id;
                    var device_number = parseInt(device_id.substr(-2));

                    var geometry = new THREE.SphereGeometry(0.1, 6, 6);
                    var material = new THREE.MeshBasicMaterial({color: 0xffff00});
                    var sphere = new THREE.Mesh(geometry, material);
                    sphere.translateX(device_position.x);
                    sphere.translateY(device_position.y);
                    sphere.translateZ(device_position.z);
                    sphere.renderDepth = 2;
                    scene.the_scene.add(sphere);


                    $.ajax('/amon/devices', {
                        type: 'POST',
                        contentType: 'application/json',
                        dataType: 'json',
                        data: JSON.stringify({'deviceId': device_id,
                            'privacy': device_mesh.userData.privacy,
                            'meteringPointId': mesh.userData.id,
                            'entityId': mesh.parent.uuid,
                            'description': device_mesh.userData.description,
                            'metadata': {'position': [device_position.x, device_position.y, device_position.z]}
                        }),
                        success: function() {console.log('Added new device to database.')}
                    });

                    var value = 23.0;

                    sensor_heirarchy += '<li style="display: none;"><span class="btn-success">' + (device_number + 1).toString();
                    sensor_heirarchy += ' <i class="fa fa-fw fa-lg ' + sensor_icons[device_mesh.userData.type] + '"></i> ';
                    sensor_heirarchy += '<i class="fa ' + trend_icons[1] + '"></i>&emsp;' + value.toFixed(1) + sensor_units[device_mesh.userData.type] + ' </span></li>';

                    switch (device_mesh.userData.type) {
                        case 'temperatureAir':
                            temperatureAir.push({id: device_id, position: device_position});
                            break;
                    }
                }
            }

            sensor_heirarchy += '</ul>';
            parent.append(sensor_heirarchy);

            return temperatureAir;
        }

        function computeSceneBoundingSphere(root) {

            root.traverse(function (object) {
                if (object instanceof THREE.Mesh) {
                    _parse_entity(object);
                }
            } );

            return new THREE.Sphere(new THREE.Vector3(0.0, 0.0, 0.0), 10.3);
        }

        function on_load_complete() {
            // TODO: Why does reloading the model not work?
            scene.sensor_positions.length = 0;
            scene.room_meshes.length = 0;
            scene.bounding_sphere = computeSceneBoundingSphere(scene.the_scene);
            $('span.entity').on('click', expand_sidebar);

            function bar(e) {
                expand_sidebar(e);
                var room_idx = parseInt(this.control.value);
                var start = controls.target.clone();
                var centre = scene.room_meshes[room_idx].geometry.boundingSphere.center.clone();
                var tween = new TWEEN.Tween(start).to(centre, 800);
                tween.onUpdate(function() {
                    controls.target.x = start.x;
                    controls.target.y = start.y;
                    controls.target.z = start.z;
                });
                tween.easing(TWEEN.Easing.Quartic.Out);
                tween.start();

                if (selected_object !== undefined) {
                    selected_object.material.uniforms.selected.value = 0;
                }

                selected_object = scene.room_meshes[room_idx];
                selected_object.material.uniforms.selected.value = 1;
            }

            var room_selector = $('label.room');

            room_selector.on('click', bar);

            room_selector.on('mouseenter', function(e) {
                if (highlighted_object !== undefined) {
                    highlighted_object.material.uniforms.highlighted.value = 0;
                }

                var room_idx = parseInt(this.control.value);
                highlighted_object = scene.room_meshes[room_idx];
                highlighted_object.material.uniforms.highlighted.value = 1;
            });
            room_selector.on('mouseleave', function(e) {
                highlighted_object.material.uniforms.highlighted.value = 0;
                highlighted_object = undefined;
            });
            $('span.metering-point').on('click', expand_sidebar);

            // Ensure we have a power of two-sized texture.
            var num_rooms = Math.pow(2, Math.ceil(Math.log(scene.room_meshes.length) / Math.log(2)));

            var radius = 14.0;
            var max_sensors_per_room = 128;

            var pos = new Float32Array(max_sensors_per_room * num_rooms * 3);
            scene.val_buf = new Float32Array(max_sensors_per_room * num_rooms);

            textures.positions = new THREE.DataTexture(pos, max_sensors_per_room, num_rooms, THREE.RGBFormat, THREE.FloatType, new THREE.UVMapping(), THREE.ClampToEdgeWrapping, THREE.ClampToEdgeWrapping, THREE.NearestFilter, THREE.NearestFilter, 1);
            textures.values = new THREE.DataTexture(scene.val_buf, max_sensors_per_room, num_rooms, THREE.LuminanceFormat, THREE.FloatType, new THREE.UVMapping(), THREE.ClampToEdgeWrapping, THREE.ClampToEdgeWrapping, THREE.NearestFilter, THREE.NearestFilter, 1);

            materials.temperatureAir_shader.uniforms.radius.value = radius;
            materials.temperatureAir_shader.needsUpdate = true;

            for (var room_idx = 0; room_idx < scene.room_meshes.length; room_idx++) {
                var object = scene.room_meshes[room_idx];

                if (object == undefined) {
                    continue;
                }

                // Cloning a shader material with textures also clones those textures, which is not what we want.
                object.material = materials.temperatureAir_shader.clone();
                object.material.uniforms.sensor_positions.value = textures.positions;
                object.material.uniforms.sensor_values.value = textures.values;
                object.material.uniforms.gradient.value = textures.gradient;

                // Iterate over each sensor position.
                object.material.uniforms.num_sensors.value = scene.sensor_pos[object.uuid].length;
                object.material.uniforms.room_texture_v.value = 1.0 - ((room_idx + 0.5) / num_rooms);

                for (var sensor_idx = 0; sensor_idx < scene.sensor_pos[object.uuid].length; sensor_idx++) {
                    var idx = (room_idx * max_sensors_per_room) + sensor_idx;
                    pos[(idx * 3) + 0] = ((scene.sensor_pos[object.uuid][sensor_idx].position.x / radius) + 1.0) / 2.0;
                    pos[(idx * 3) + 1] = ((scene.sensor_pos[object.uuid][sensor_idx].position.y / radius) + 1.0) / 2.0;
                    pos[(idx * 3) + 2] = ((scene.sensor_pos[object.uuid][sensor_idx].position.z / radius) + 1.0) / 2.0;
                    scene.val_buf[idx] = 0.5;
                }
            }

            textures.positions.needsUpdate = true;
            textures.values.needsUpdate = true;

            $(scene).trigger('reloaded');
            scene.get_data(start_date);
        }

        var scene = {
            max_sensors_per_room: 128,
            val_buf: new Float32Array(1),
            the_scene: new THREE.Scene(),
            bounding_sphere: new THREE.Sphere(),
            sensor_positions: [],
            // Map sensor UUIDs to the order they were loaded in from the model, so we can associate a sensor with the right position in the world.
            room_meshes: [],
            sensor_pos: {},
            next_date: start_date,
            playing: false,
            load: function(model_id) {
                $(scene).trigger('loading');

                $.get('/housahedron/scene/' + model_id, function(data) {
                    var j = new THREE.ObjectLoader();
                    scene.the_scene = j.parse(data);

                    $.get('/housahedron/edges/' + model_id, function(data) {
//                        var m = new THREE.LineBasicMaterial({color: 0xffc931});
                        var m = new THREE.LineBasicMaterial({color: 0x31c9ff});
                        m.lineWidth = 5.0;
                        var o = new THREE.Object3D();
                        o.name = 'wireframe';

                        var t = data.transform;
                        o.applyMatrix(new THREE.Matrix4(t[0], t[4], t[8], t[12],
                                                        t[1], t[5], t[9], t[13],
                                                        t[2], t[6], t[10], t[14],
                                                        t[3], t[7], t[11], t[15]));

                        for (var i = 0; i < data.vertices.length; i++) {
                            var g = new THREE.Geometry();
                            var v1 = data.vertices[i][0];
                            var v2 = data.vertices[i][1];
                            g.vertices.push(new THREE.Vector3(v1[0], v1[1], v1[2]));
                            g.vertices.push(new THREE.Vector3(v2[0], v2[1], v2[2]));
                            var l = new THREE.Line(g, m);
                            o.renderDepth = 3;
                            l.renderDepth = 3;
                            o.add(l);
                        }

                        scene.the_scene.add(o);

                        on_load_complete();
                    });
                });
            },
            get_data: function(date) {
                var start = new Date(date);
                start.setUTCHours(date.getUTCHours() - 1);

                $.get('/housahedron/data', {end: date.toISOString(), start: start.toISOString()}, function(data) {
                    data['results'].forEach(function(entry) {
                        var found = false;
                        // Gets the index into the sensor values texture for a given sensor UUID.
                        // Bit of a brute-force search, but with a modest number of rooms, should be no biggie.
                        var room_idx = 0;

                        for (var room in scene.sensor_pos) {
                            for (var sensor_idx = 0; sensor_idx < scene.sensor_pos[room].length; sensor_idx++) {
                                if (scene.sensor_pos[room][sensor_idx].id === entry[0]) {
                                    var idx = (room_idx * scene.max_sensors_per_room) + sensor_idx;
                                    var value = entry[2];
                                    var mean = 23.0;
                                    var one_sigma = 3.133;
                                    var min = mean - (1.5 * one_sigma);
                                    var max = mean + (1.5 * one_sigma);
                                    scene.val_buf[idx] = (value - min) / (max - min);
                                    found = true;
                                    break;
                                }
                            }

                            if (found) {
                                break;
                            }

                            room_idx++;
                        }

//                        if (!found) {
//                            console.log('Sensor ' + entry[0] + ' not found in model');
//                        }
                    });

                    if (textures.values !== null) {
                        textures.values.needsUpdate = true;
                    }

                    $('#status').html('<h2>' + date.toUTCString() + '</h2>');
                });
            },
            set_date: function (date) {
                scene.next_date = date;

                if (scene.next_date.getTime() > end_date.getTime()) {
                    scene.next_date = start_date;
                } else if (scene.next_date.getTime() < start_date.getTime()) {
                    scene.next_date = end_date;
                }

                scene.get_data(scene.next_date);

                $(scene).trigger('date_changed', scene.next_date);
            }
        };

        return scene;
    }
);
