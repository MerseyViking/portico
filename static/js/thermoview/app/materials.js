define(["threeCore", "shader!back.vert", "shader!back.frag"], function (THREE, back_vert, back_frag) {
    return {
        temperatureAir_shader: new THREE.ShaderMaterial({
            transparent: true,
            depthTest: true,
            depthWrite: true,
            side: THREE.DoubleSide,
            uniforms: {
                centre: {type: 'v3', value: new THREE.Vector3(0.0, 0.0, 0.0)},
                radius: {type: 'f', value: 14.0},

                gradient: {type: 't', value: null},
                sensor_positions: {type: 't', value: null},
                sensor_values: {type: 't', value: null},
                num_sensors: {type: 'i', value: 0},
                room_texture_v: {type: 'f', value: 0.0},
                selected: {type: 'i', value: 0},
                highlighted: {type: 'i', value: 0},
                pulse: {type: 'f', value: 0.0}
            },
            vertexShader: back_vert.value,
            fragmentShader: back_frag.value
        })
    };
} );
