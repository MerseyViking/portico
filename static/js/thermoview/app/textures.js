define(["threeCore"], function (THREE) {
    var texturePath = "static/js/thermoview/textures/";

    var textures = {
      gradient: THREE.ImageUtils.loadTexture(texturePath + "gradient_1.png"),
//      gradient: THREE.ImageUtils.loadTexture(texturePath + "random.png"),
      positions: null,
      values: null
    };

    return textures;
});
