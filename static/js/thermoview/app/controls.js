define(['threeCore', 'camera', 'container', 'scene', 'OrbitControls'],
    function(THREE, camera, container, scene) {
        var orbit = new THREE.OrbitControls(camera, container);
        orbit.autoRotate = false;
        orbit.noPan = false;
        orbit.noRotate = false;

        $(scene).on('reloaded', function() {
            // camera.fov == vertical FoV.
            // TODO: Take into account aspect ratio.
            var tfov = Math.tan(camera.fov / 2.0);
            var dist = scene.bounding_sphere.radius / tfov;

            var cur_vec = new THREE.Vector3();
            cur_vec.subVectors(camera.position, orbit.target);
            cur_vec.setLength(dist);

            orbit.target = scene.bounding_sphere.center;
            camera.position.addVectors(orbit.target, cur_vec);
        });

        return orbit;
    }
);