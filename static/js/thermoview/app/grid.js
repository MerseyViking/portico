define(['threeCore', 'scene'],
    function(THREE, scene) {
        var grid = {
            grid_extent: 40,
            grid_major: 5.0,
            grid_minor: 1.0
        };

        $(scene).on('reloaded', function() {
            var majorGridLines = new THREE.Geometry();
//            var minorGridLines = new THREE.Geometry();

            var half_size = (grid.grid_extent / grid.grid_minor) / 2;

            for (var x = -half_size; x <= half_size; ++x) {
                if (x % (grid.grid_major / grid.grid_minor) == 0) {
                    majorGridLines.vertices.push(new THREE.Vector3(x * grid.grid_minor, 0.0, -grid.grid_minor * half_size));
                    majorGridLines.vertices.push(new THREE.Vector3(x * grid.grid_minor, 0.0, grid.grid_minor * half_size));

                    majorGridLines.vertices.push(new THREE.Vector3(-grid.grid_minor * half_size, 0.0, x * grid.grid_minor));
                    majorGridLines.vertices.push(new THREE.Vector3(grid.grid_minor * half_size, 0.0, x * grid.grid_minor));
//                } else {
//                    minorGridLines.vertices.push(new THREE.Vector3(x * grid.grid_minor, 0.0, -grid.grid_minor * half_size));
//                    minorGridLines.vertices.push(new THREE.Vector3(x * grid.grid_minor, 0.0, grid.grid_minor * half_size));
//
//                    minorGridLines.vertices.push(new THREE.Vector3(-grid.grid_minor * half_size, 0.0, x * grid.grid_minor));
//                    minorGridLines.vertices.push(new THREE.Vector3(grid.grid_minor * half_size, 0.0, x * grid.grid_minor));
                }
            }

            var major_lines = new THREE.Line(majorGridLines, new THREE.LineBasicMaterial({color: 0xb0a574}), THREE.LinePieces);
//            var minor_lines = new THREE.Line(minorGridLines, new THREE.LineBasicMaterial({color: 0xfcec95}), THREE.LinePieces);

            var plane_geometry = new THREE.PlaneGeometry(grid.grid_extent, grid.grid_extent);
            var plane_material = new THREE.MeshBasicMaterial({color: 0x2f2640, side: THREE.FrontSide});
            var plane = new THREE.Mesh(plane_geometry, plane_material);
            plane.translateX(-2.5);
            plane.translateY(-0.025);
            plane.translateZ(-6.5);
            plane.rotateOnAxis(new THREE.Vector3(1.0, 0.0, 0.0), -Math.PI / 2.0);
            plane.renderDepth = 0;

            var grid_root = new THREE.Object3D();
            grid_root.name = "grid-root";

//            grid_root.add(minor_lines);
            grid_root.add(major_lines);
            grid_root.renderDepth = 0;
            grid_root.translateX(-2.5);
            grid_root.translateY(-0.02);
            grid_root.translateZ(-6.5);

            scene.the_scene.add(grid_root);
            scene.the_scene.add(plane);
        });

        return grid;
    }
);