define(['threeCore', 'scene', 'controls', 'camera', 'sparklines'],
    function(THREE, scene, controls, camera, sparklines) {

        var weather_icons = {
            'Clear': '2',
            'Drizzle': '17',
            'Fog': '13',
            'Haze': '5',
            'Heavy Drizzle': '17',
            'Heavy Thunderstorms and Rain': '27',
            'Light Drizzle': '17',
            'Light Rain': '18',
            'Light Rain Showers': '20',
            'Mist': '1',
            'Mostly Cloudy': '14',
            'Overcast': '25',
            'Partly Cloudy': '8',
            'Patches of Fog': '10',
            'Rain': '18',
            'Rain Showers': '20',
            'Scattered Clouds': '8',
            'Shallow Fog': '10',
            'Thunderstorm': '16',
            'Thunderstorms and Rain': '16',
            'Sleet': '24',
            'Snow': '22',
            'Heavy Snow': '23'
        };

        var last_icon = '';

        $(scene).on('date_changed', function(event, date) {
            if (date.getUTCMinutes() === 0) {
                $.get('/housahedron/weather', {start: date.toISOString()}, function(data) {
                    if (last_icon !== weather_icons[data.results[0][2]]) {
                        var icon = '/static/img/weather/' + weather_icons[data.results[0][2]] + '.svg';
                        var svg = '<object type="image/svg+xml" width="64px" height="64px" data="' + icon + '">' + data.results[0][2] + '</object>';
                        last_icon = weather_icons[data.results[0][2]];
                        $('.weather#weather-icon').html(svg);
                    }

                    $('.weather#temperature').html('<h1>' + data.results[0][1] + '&deg;C</h1>');
                });
            }
        });

//        var refresh_button = $('button#refresh');
        var update_button = $('button#update');

//        $(scene).on('reloaded', function() {
//            refresh_button.children(':first').removeClass('fa-spin');
//        });
//
//        $(scene).on('loading', function() {
//            refresh_button.children(':first').addClass('fa-spin');
//        });

//        refresh_button.click(function() {
////            scene.load('aafaffe1-fe43-4b99-b693-a6013011b3f7');
//            controls.autoRotate = !controls.autoRotate;
//            controls.noRotate = !controls.noRotate;
//            console.log(controls.autoRotate);
//        });

        update_button.click(function() {
            controls.autoRotate = !controls.autoRotate;
            controls.noRotate = !controls.noRotate;
        });

        $('button#month-back').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCMonth(date.getUTCMonth() - 1);
            scene.set_date(date);
        });

        $('button#day-back').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCDate(date.getUTCDate() - 1);
            scene.set_date(date);
        });

        $('button#hour-back').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCHours(date.getUTCHours() - 1);
            scene.set_date(date);
        });

        $('button#month-forward').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCMonth(date.getUTCMonth() + 1);
            scene.set_date(date);
        });

        $('button#day-forward').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCDate(date.getUTCDate() + 1);
            scene.set_date(date);
        });

        $('button#hour-forward').click(function() {
            var date = new Date(scene.next_date);
            date.setUTCHours(date.getUTCHours() + 1);
            scene.set_date(date);
        });

        $('button#play-pause').click(function() {
            scene.playing = !scene.playing;
            var icon = $('button#play-pause > i');

            if (scene.playing) {
                icon.removeClass('fa-play');
                icon.addClass('fa-pause');
            } else {
                icon.removeClass('fa-pause');
                icon.addClass('fa-play');
            }
        });

//        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
//
//        $('#slider').dateRangeSlider({
//            bounds: {min: new Date(2014, 0, 1), max: new Date(2014, 6, 4)},
//            defaultValues: {min: new Date(2014, 6, 1), max: new Date(2014, 6, 4)},
//            step: {days: 1},
//            formatter: function(val) {
//                var cur_values = $('#slider').dateRangeSlider('values');
//
//                // Because the range slider *includes* the last day, we want to *exclude* it.
//                if (+cur_values.max === +val) {
//                    val.setDate(val.getDate() - 1);
//                }
//
//                var year = val.getFullYear();
//                var month = val.getMonth();
//                var day = val.getDate();
//                return day.toString() + ' ' + months[month] + ', ' + year.toString();
//            }
//        }).bind('valuesChanged', function(event, daterange) {
//            scene.get_data(daterange.values.max);
//        });
//        $('#slider').dateRangeSlider(
//            {
//                bounds: {min: new Date(2014, 0, 1), max: new Date(2014, 6, 4)},
//                defaultValues: {min: new Date(2014, 0, 1), max: new Date(2014, 6, 4)},
//                step: {months: 6},
//                scales: [{
//                    first: function(value){return value;},
//                    end: function(value) {return value;},
//                    next: function(value){
//                        var next = new Date(value);
//                        return new Date(next.setFullYear(value.getFullYear() + 1));
//                    },
//                    label: function(value) {
//                        return value.getFullYear();
//                    },
//                    format: function(tickContainer, tickStart, tickEnd) {
//                        if (tickStart.getFullYear() % 2 == 0) {
//                            tickContainer.addClass("even-year");
//                        } else {
//                            tickContainer.addClass("odd-year");
//                        }
//                        tickContainer.addClass("text-white-outline");
//                    }
//                }],
//                formatter: function(val) {
//                    var cur_values = $('#slider').dateRangeSlider('values');
//
//                    // Because the range slider *includes* the last day, we want to *exclude* it.
//                    if (+cur_values.max === +val) {
//                        val.setDate(val.getDate() - 1);
//                    }
//
//                    var year = val.getFullYear();
//                    var month = val.getMonth();
//                    return months[month] + ', ' + year.toString();
//                }
//            }
//        );

//        $('#month-back').tooltip();

        function build_table(stats) {
            var html = '<table><tbody>';

            for (var stat in stats) {
                if (stats.hasOwnProperty(stat)) {
                    if (stat === 'Temperature') {
                        html += '<tr><td>' + stat + '</td><td style="color: #ff6050;">' + stats[stat] + '</td></tr>';
                    } else {
                        html += '<tr><td>' + stat + '</td><td>' + stats[stat] + '</td></tr>';
                    }
                }
            }

            html += '</tbody></table>';
            return html;
        }

        function update() {
            var view = $('#threejs-container');
            var widthHalf = view.width() / 2;
            var heightHalf = view.height() / 2;

            var projector = new THREE.Projector();

            hud.alerts[0] = new Object();

            hud.alerts[0].position = new THREE.Vector3(-2.682499676942825, 3.537899911403656, -11.609700202941895);
            hud.alerts[0].title = "Warning";
            hud.alerts[0].location = "Mezzanine";
            hud.alerts[0].body = build_table({
                Temperature: '27&deg;C',
                Humidity: '64% (R/H)'
            });

            var callout = $('#test-callout');

            for (var a = 0; a < hud.alerts.length; a++) {
                var vector = hud.alerts[a].position.clone();
                projector.projectVector(vector, camera);

                vector.x = -(vector.x * widthHalf) + widthHalf;
                vector.y = (vector.y * heightHalf) + heightHalf + 64;

                callout.css('right', vector.x);
                callout.css('bottom', vector.y);
                $('#test-callout > .callout-title').html(hud.alerts[a].title);
                $('#test-callout > .callout-location').html(hud.alerts[a].location);
                $('#test-callout > .callout-body').html(hud.alerts[a].body);

                var geometry = new THREE.SphereGeometry(0.1, 6, 6);
                var material = new THREE.MeshBasicMaterial({color: 0xff6050});
                var sphere = new THREE.Mesh(geometry, material);
                sphere.translateX(hud.alerts[a].position.x);
                sphere.translateY(hud.alerts[a].position.y);
                sphere.translateZ(hud.alerts[a].position.z);
                scene.the_scene.add(sphere);
                $(".sparkline").sparkline([27, 22, 15, 30, 19, 24, 14, 10, 25, 13, 12, 16, 20, 23, 26, 29, 11, 28, 21, 18, 17], {
                    type: 'line',
                    width: 200,
                    fillColor: undefined,
                    spotRadius: 2.5,
                    chartRangeMin: 0,
                    chartRangeMax: 30
                });
            }
        }

        var hud = {
            update: update,
            update_button: update_button,
            alerts: []
        };

        return hud;
    }
);