define(['threeCore', 'container'], function(THREE, container) {
    var renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x000028, 1.0);
    renderer.sortObjects = true;
    renderer.autoClear = true;
    renderer.antialias = false;
    container.appendChild(renderer.domElement);

    var updateSize = function () {
        renderer.setSize(container.offsetWidth, container.offsetHeight);
    };

    window.addEventListener('resize', updateSize, false);
    updateSize();

    return renderer;
} );