function getDaysInMonth(month,year) {
    if( typeof year == "undefined") year = 1999; // any non-leap-year works as default
    var currmon = new Date(year,month),
        nextmon = new Date(year,month+1);
    return Math.floor((nextmon.getTime()-currmon.getTime())/(24*3600*1000));
}

function getDateTimeSince(target) { // target should be a Date object
    var now = new Date(), diff, yd, md, dd, hd, nd, sd, out = [];
//    diff = Math.floor(now.getTime()-target.getTime()/1000);
    yd = now.getFullYear()-target.getFullYear();
    md = now.getMonth()-target.getMonth();
    dd = now.getDate()-target.getDate();
    hd = now.getHours()-target.getHours();
    nd = now.getMinutes()-target.getMinutes();
    sd = now.getSeconds()-target.getSeconds();

    if( sd < 0) {nd--; sd += 60;}
    if( nd < 0) {hd--; nd += 60;}
    if( hd < 0) {dd--; hd += 24;}
    if( dd < 0) {
        md--;
        dd += getDaysInMonth(now.getMonth()-1,now.getFullYear());
    }
    if( md < 0) {yd--; md += 12;}

    // Only print the highest two orders of magnitude.
    if (yd > 0) out.push( yd+" year"+(yd == 1 ? "" : "s"));
    if (md > 0) out.push( md+" month"+(md == 1 ? "" : "s"));
    if(yd <= 0 && dd > 0) out.push( dd+" day"+(dd == 1 ? "" : "s"));
    if(md <= 0 && hd > 0) out.push( hd+" hour"+(hd == 1 ? "" : "s"));
    if(dd <= 0 && nd > 0) out.push( nd+" minute"+(nd == 1 ? "" : "s"));
    if(hd <= 0 && sd > 0) out.push( sd+" second"+(sd == 1 ? "" : "s"));
    return out.join(" ");
}

$(function () {
/*
    $.get('_sensor_network', function(data) {
        // If this fails, just update the "time since", and don't clear the whole DIV.
        var sensor_heirarchy = '<ul class="nav">';
        var entities = Object.keys(data);

        for (var ei = 0; ei < entities.length; ei++) {
            var entity = data[entities[ei]];

            sensor_heirarchy += '<li class="parent_li"><span class="btn-success"><i class="fa fa-fw fa-lg fa-building-o"></i> ' + entity.description + '</span><ul>';

            var metering_point_ids = Object.keys(entity.metering_points);

            for (var mi = 0; mi < metering_point_ids.length; mi++) {
                var metering_point = entity.metering_points[metering_point_ids[mi]];
                var ts = getDateTimeSince(new Date(metering_point.last_updated));
                sensor_heirarchy += '<li class="parent_li"><span class="btn-success"><i class="fa fa-fw fa-lg fa-code-fork"></i> ' + metering_point.description + '</br>' + ts + ' ago</span><ul>';

                for (var di = 0; di < metering_point.devices.length; di++) {
                    var device = metering_point.devices[di];
                    var value = Math.round(device.value * 10.0) / 10.0;
                    var trend_icon = trend_icons[device.trend < 0.0 ? 0 : device.trend > 0.0 ? 2 : 1];
                    sensor_heirarchy += '<li style="display: none;"><span class="btn-success">' + (device.number + 1).toString();
                    sensor_heirarchy += ' <i class="fa fa-fw fa-lg ' + sensor_icons[device.type] + '"></i> ';
                    sensor_heirarchy += '<i class="fa ' + trend_icon + '"></i>&emsp;' + value.toFixed(1) + sensor_units[device.type] + ' </span></li>';
                }

                sensor_heirarchy += '</ul></li>';
            }

            sensor_heirarchy += '</ul></li>';
        }

        sensor_heirarchy += '</ul>';

        document.getElementById('sensor-heirarchy').innerHTML = sensor_heirarchy;
*/
//        $('.tree li.parent_li > span').on('click', function (e) {
//        $('span.metering-point').on('click', function (e) {
//            var children = $(this).parent('li.parent_li').find(' > ul > li');
//            if (children.is(":visible")) {
//                children.hide('fast');
//            } else {
//                children.show('fast');
//            }
//            e.stopPropagation();
//        });
//    });

});


function expand_sidebar(e) {
    var children = $(this).parent('li.parent_li').find(' > ul > li');
    if (children.is(":visible")) {
        children.hide('fast');
    } else {
        children.show('fast');
    }

    e.stopPropagation();
}