# Copyright (c) 2014 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT

from flask import Flask
from flask.ext.restful import Api
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

app = Flask(__name__)
api = Api(app)
engine = create_engine('sqlite:///database/housahedron.db', echo=False)
session_factory = sessionmaker(bind=engine)
session = session_factory()
