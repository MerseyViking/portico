# Copyright (c) 2014 GeoSpark
#
# Released under the MIT License (MIT)
# See the LICENSE file, or visit http://opensource.org/licenses/MIT
from collections import defaultdict
import gzip

import numpy as np

import json
import math


def _get_transform(scene, uuid, matrix):
    children = scene.get('children', [])

    for child in children:
        node_matrix = np.asarray(child['matrix'])
        foo = node_matrix.reshape((4, 4))
        concat_matrix = matrix.dot(foo)

        if child.get('geometry', '') == uuid:
            return concat_matrix

        return _get_transform(child, uuid, concat_matrix)

    return matrix


def find_edges(model_id):
    with gzip.open('.cache/{0}.json.gz'.format(model_id), 'rb') as f:
        scene = json.load(f)

    edges = []
    node_xform = np.identity(4)

    for geometry in scene['geometries']:
        if len(geometry['data']['vertices']) == 0:
            continue

        my_faces = []
        my_normals = []
        my_edges = defaultdict(list)

        faces = geometry['data']['faces']
        normals = geometry['data']['normals']
        root_xform = np.asarray(scene['object']['matrix']).reshape((4, 4))
        # node_xform = _get_transform(scene['object'], geometry['uuid'], root_xform)

        vertices = zip(*[iter(geometry['data']['vertices'])] * 3)

        # foo = np.asarray(geometry['data']['vertices'])
        # foo = np.reshape(foo, (len(foo) / 3, 3))
        # vertices = np.hstack((foo, np.ones((foo.shape[0], 1))))
        # vertices = node_xform.dot(vertices[..., np.newaxis].transpose()).transpose().squeeze(axis=(1,))

        face_idx = 0

        while True:
            face = faces[face_idx:]

            if len(face) == 0:
                break

            descriptor = face[0]
            index_count = 1

            if descriptor & 1 != 0:
                vertex_count = 4
            else:
                vertex_count = 3

            face_vertices = face[index_count:vertex_count + 1]

            for v in range(0, vertex_count):
                v1 = face_vertices[v]
                v2 = face_vertices[(v + 1) % vertex_count]
                f = len(my_faces)
                my_edges[(min(v1, v2), max(v1, v2))].append(f)

            my_faces.append(face_vertices)

            index_count += vertex_count

            # Material
            if descriptor & 2 != 0:
                index_count += 1

            # Face UV
            if descriptor & 4 != 0:
                index_count += 1

            # Vertex UV
            if descriptor & 8 != 0:
                index_count += vertex_count

            # Face normal
            if descriptor & 16 != 0:
                normal_index = face[index_count] * 3

                my_normals.append(normals[normal_index:normal_index + 3])
                index_count += 1

            # Vertex normal
            if descriptor & 32 != 0:
                index_count += vertex_count

            # Face colour
            if descriptor & 64 != 0:
                index_count += 1

            # Vertex colour
            if descriptor & 128 != 0:
                index_count += vertex_count

            face_idx += index_count

        for indices, faces in my_edges.iteritems():
            if len(faces) == 1:
                # edges.append((vertices[indices[0]][:3].tolist(), vertices[indices[1]][:3].tolist()))
                edges.append((vertices[indices[0]], vertices[indices[1]]))
            elif len(faces) == 2:
                n1 = my_normals[faces[0]]
                n2 = my_normals[faces[1]]
                d = min(max((n1[0] * n2[0]) + (n1[1] * n2[1]) + (n1[2] * n2[2]), -1.0), 1.0)
                a = math.acos(d)

                if math.radians(15.0) < math.fabs(a):
                    # edges.append((vertices[indices[0]][:3].tolist(), vertices[indices[1]][:3].tolist()))
                    edges.append((vertices[indices[0]], vertices[indices[1]]))

    return {'transform': node_xform.ravel().tolist(), 'vertices': edges}